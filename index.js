const express = require('express');
const path = require('path')
const app = express();

// Define the port to run on
app.set('port', 3000);

app.use('/public', express.static(path.join(__dirname + '/public')));

// Listen for requests
const server = app.listen(app.get('port'), function () {
	const port = server.address().port;
	console.log('Open http://localhost:' + port + '/public/index.html');
});